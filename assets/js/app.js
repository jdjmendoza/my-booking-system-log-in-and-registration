const logInBtn = document.querySelector("#log-in-btn");
const registerBtn = document.querySelector("#register-btn");
const container = document.querySelector(".container");

registerBtn.addEventListener("click", () => {
  container.classList.add("register-mode");
});

logInBtn.addEventListener("click", () => {
  container.classList.remove("register-mode");
});
