let loggedInCredentials = localStorage.getItem("id")

if(loggedInCredentials) {
  console.log(localStorage);
  document.getElementById('edit-first-name').value = localStorage.getItem('firstName');
  document.getElementById('edit-last-name').value = localStorage.getItem('lastName');
  document.getElementById('edit-email').value = localStorage.getItem('email');
  document.getElementById('course-profile').value = localStorage.getItem('status') == "true" ? "Enrolled" : "Not Enrolled"
  document.getElementById('edit-mobile').value = localStorage.getItem('mobileNo');
} else {
  window.location.replace("../index.html")
}