let registerForm = document.getElementById('registerForm');

  //if registerForm is submitted
registerForm.addEventListener('submit', (e) => {
  
  e.preventDefault()

  //assign tge values firstname and lastname to variables below
  let firstName = document.getElementById('firstName').value;
  let lastName = document.getElementById('lastName').value;
  let email = document.getElementById('email').value;
  let password = document.getElementById('password').value;
  let mobileNo = document.getElementById('mobileNo').value;
  let courseId = document.getElementById('sel1').value;

  //if firstname is an empty string or lastname is an empty string
  if(firstName == "" || lastName == "" || email == "" || password == "" || mobileNo == "" || courseId == "") {
    //prompt 
    alert("all fields are required")
  }
  else {
    
    fetch('https://mysterious-waters-19202.herokuapp.com/api/users/register', {
      method: 'POST',
      headers: { 
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        mobileNo: mobileNo,
        courseId: courseId
      })
    })
    .then(res => {
      return res.json()
    })
    .then(data => {
      if (data.user) {
          localStorage.setItem("id", data.user._id)
          localStorage.setItem("firstName", data.user.firstName)
          localStorage.setItem("lastName", data.user.lastName)
          localStorage.setItem("email", data.user.email)
          localStorage.setItem("mobileNo", data.user.mobileNo)
          localStorage.setItem("courseId", data.user.enrollments[0].courseId)
          localStorage.setItem("status", data.user.enrollments[0].isCurrentlyEnrolled)
      
          window.location.replace("./pages/profile-page.html")
        } else {
          alert("Oops! Something went wrong!")
        } 

    })

  }
})