let editForm = document.getElementById('edit-form');

  //if editForm is submitted
editForm.addEventListener('submit', (e) => {
  console.log("test");
  e.preventDefault();

  //assign tge values firstname and lastname to variables below
  let firstName = document.getElementById('edit-first-name').value;
  let lastName = document.getElementById('edit-last-name').value;
  let email = document.getElementById('edit-email').value;
  let password = document.getElementById('edit-password').value;
  let confirmPassword = document.getElementById('confirm-password').value;
  let mobileNo = document.getElementById('edit-mobile').value;
  let courseId = document.getElementById('edit-course').value;

  if(password != confirmPassword) {
    alert("Passwords does not match")
  } else if(firstName == "" || lastName == "" || email == "" || mobileNo == "" || courseId == "") {
    alert("All fields are required")
  } else {
    fetch(`https://mysterious-waters-19202.herokuapp.com/api/users/${loggedInCredentials}`, {
      method: 'PUT',
      headers: { 
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        mobileNo: mobileNo,
        courseId: courseId
      })
    })
    .then(res => {
      return res.json()
    })
    .then(data => {
      if (data.user) {
          localStorage.setItem("firstName", data.user.firstName)
          localStorage.setItem("lastName", data.user.lastName)
          localStorage.setItem("email", data.user.email)
          localStorage.setItem("mobileNo", data.user.mobileNo)
          localStorage.setItem("courseId", data.user.enrollments[0].courseId)
          localStorage.setItem("status", data.user.enrollments[0].isCurrentlyEnrolled)
      
          window.location.replace("../index.html")
          alert("Successfully updated!")
        } else {
          alert("Oops! Something went wrong!")
        } 

    })
  }
})