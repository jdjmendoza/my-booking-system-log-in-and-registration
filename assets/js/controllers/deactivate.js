let logout = document.getElementById('deactivate-btn')

logout.addEventListener("click", () => {
  fetch(`https://mysterious-waters-19202.herokuapp.com/api/users/${loggedInCredentials}/deactivate`, {
    method: 'POST',
    headers: { 
      'Content-Type': 'application/json'
    }
  })
  .then(res => {
    return res.json()
  })
  .then(data => {
    if(data.success) {
      alert("User deactivated. You will be logged out.")
      localStorage.clear();
      window.location.replace("../index.html")
    } else {
      alert(data.message)
    }
  })
})