let loggedInCredentials = localStorage.getItem("id")

if(!loggedInCredentials){
  let loginForm = document.getElementById('loginForm');

  //if loginForm is submitted
  loginForm.addEventListener('submit', (e) => {
    e.preventDefault()

    //assign tge values firstname and lastname to variables below
    let email = document.getElementById('emailLogin').value;
    let password = document.getElementById('passwordLogin').value;
    // let courseId = document.getElementById('sel1').value;

    //if firstname is an empty string or lastname is an empty string
    if(email == "" || password == "") {
      //prompt 
      alert("all fields are required")
    }
    else {
      fetch('https://mysterious-waters-19202.herokuapp.com/api/users/login', {
        method: 'POST',
        headers: { 
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res => {
        return res.json()
      })
      .then(data => {
        if (data.user) {
          if (data.user.isActive) {
            localStorage.setItem("id", data.user._id)
            localStorage.setItem("firstName", data.user.firstName)
            localStorage.setItem("lastName", data.user.lastName)
            localStorage.setItem("email", data.user.email)
            localStorage.setItem("mobileNo", data.user.mobileNo)
            localStorage.setItem("courseId", data.user.enrollments[0].courseId)
            localStorage.setItem("status", data.user.enrollments[0].isCurrentlyEnrolled)

            window.location.replace("./pages/profile-page.html")
          } else {
            alert("Oops! User deactivated!")
          }
        } else {
          alert("Oops! Email is not registered or does not match password!")
        } 

      })

    }
  })
} else {
  window.location.replace("./pages/profile-page.html")
}