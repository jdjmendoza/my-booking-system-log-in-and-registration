let listCoursesContainer = document.getElementById('sel1') 
if (loggedInCredentials) {
  listCoursesContainer = document.getElementById('edit-course')
}
// let loggedInCredentials = localStorage.getItem("id")
let courseId = localStorage.getItem("courseId")

fetch('https://mysterious-waters-19202.herokuapp.com/api/courses')
  .then(response => response.json())
  .then((data) => {

    let courseData;

    if (data.length < 1) {
      courseData = "No courses created yet."
    } else {
      let pre = loggedInCredentials ? `<option disabled hidden value="">Course</option>` : `<option disabled selected hidden value="">Course</option>`
      courseData = data.map(course => {
        if (loggedInCredentials && course._id == courseId) {
          return `<option selected value='${course._id}'>${course.name}</option>`
        } else {
          return `<option value='${course._id}'>${course.name}</option>`
        } 
      }).join("")
      courseData = pre + courseData

    }
    listCoursesContainer.innerHTML = courseData;
  }).catch(error => {
    console.log(error)
  })